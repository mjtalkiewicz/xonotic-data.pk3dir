#pragma once

/// \file
/// \brief Header file that describes the resource system.
/// \author Lyberta
/// \copyright GNU GPLv2 or any later version.

#include <common/resources.qh>

/// \brief Unconditional maximum amount of resources the entity can have.
const int RESOURCE_AMOUNT_HARD_LIMIT = 999;

// ============================ Public API ====================================

/// \brief Returns the maximum amount of the given resource.
/// \param[in] e Entity to check.
/// \param[in] resource_type Type of the resource (a RESOURCE_* constant).
/// \return Maximum amount of the given resource.
float GetResourceLimit(entity e, int resource_type);

/// \brief Returns the current amount of resource the given entity has.
/// \param[in] e Entity to check.
/// \param[in] resource_type Type of the resource (a RESOURCE_* constant).
/// \return Current amount of resource the given entity has.
float GetResourceAmount(entity e, int resource_type);

/// \brief Sets the current amount of resource the given entity will have.
/// \param[in,out] e Entity to adjust.
/// \param[in] resource_type Type of the resource (a RESOURCE_* constant).
/// \param[in] amount Amount of resource to set.
/// \return No return.
void SetResourceAmount(entity e, int resource_type, float amount);

/// \brief Gives an entity some resource.
/// \param[in,out] receiver Entity to give resource to.
/// \param[in] resource_type Type of the resource (a RESOURCE_* constant).
/// \param[in] amount Amount of resource to give.
/// \return No return.
void GiveResource(entity receiver, int resource_type, float amount);

/// \brief Gives an entity some resource but not more than a limit.
/// \param[in,out] receiver Entity to give resource to.
/// \param[in] resource_type Type of the resource (a RESOURCE_* constant).
/// \param[in] amount Amount of resource to give.
/// \param[in] limit Limit of resources to give.
/// \return No return.
void GiveResourceWithLimit(entity receiver, int resource_type, float amount,
	float limit);

// ===================== Legacy and/or internal API ===========================

/// \brief Converts an entity field to resource type.
/// \param[in] resource_field Entity field to convert.
/// \return Resource type (a RESOURCE_* constant).
int GetResourceType(.float resource_field);

/// \brief Converts resource type (a RESOURCE_* constant) to entity field.
/// \param[in] resource_type Type of the resource.
/// \return Entity field for that resource.
.float GetResourceField(int resource_type);
