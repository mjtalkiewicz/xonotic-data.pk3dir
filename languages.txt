ast   Asturian "Asturianu" 73%
de    German "Deutsch" 99%
de_CH German "Deutsch (Schweiz)" 99%
en    English "English"
en_AU English "English (Australia)" 86%
es    Spanish "Español" 99%
fr    French "Français" 100%
it    Italian "Italiano" 100%
hu    Hungarian "Magyar" 55%
nl    Dutch "Nederlands" 70%
pl    Polish "Polski" 80%
pt    Portuguese "Português" 99%
ro    Romanian "Romana" 84%
fi    Finnish "Suomi" 33%
zh_TW "Chinese (Taiwan)" "國語" 68%
el    Greek "Ελληνική" 33%
be    Belarusian "Беларуская" 62%
bg    Bulgarian "Български" 68%
ru    Russian "Русский" 100%
sr    Serbian "Српски" 71%
uk    Ukrainian "Українська" 57%
